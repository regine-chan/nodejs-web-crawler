const needle = require("needle");
const cheerio = require("cheerio");
const fs = require("fs");

// Fetches the webpage
needle.get("https://en.wikipedia.org/wiki/Afrikaans", {
    json: true
}, (err, res) => {
    // Prints error if error is returned
    if (err) {
        console.log("\n*************\nError\n************\n");
    }

    // fetches the body
    let body = res.body;
    // Reads content using cheerio
    const $ = cheerio.load(body);

    // Inits an object to hold values
    const elements = {
        // Fetches the title's page
        title: $('title').html(),
        safeLinkCount: 0,
        unsafeLinkCount: 0,
        safeLinks: [],
        unsafeLinks: []
    }

    // Fetches all "safe" and "unsafe" links
    $('a').each((i, link) => {
        let currentLink = $(link).attr('href');
        if (currentLink != undefined) {
            if (currentLink.includes("https")) {
                elements.safeLinks.push(currentLink);
            } else {
                elements.unsafeLinks.push(currentLink);
            }
        }
    });

    // Counts safe vs unsafe links
    elements.unsafeLinkCount = elements.unsafeLinks.length;
    elements.safeLinkCount = elements.safeLinks.length;


    // Writes to a json file
    fs.writeFile("webpage.json", JSON.stringify(elements), (err, data) => {
        if (err) {
            console.log("\n*************\nError\n************\n");
        }

        console.log(data);
    });


});